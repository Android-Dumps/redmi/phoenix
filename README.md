## qssi-user 12
12 SKQ1.210908.001 V13.0.1.0.SGHCNXM release-keys
- Manufacturer: xiaomi
- Platform: sm6150
- Codename: phoenix
- Brand: Redmi
- Flavor: qssi-user
- Release Version: 12
12
- Id: SKQ1.210908.001
- Incremental: V13.0.1.0.SGHCNXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: zh-CN
- Screen Density: 440
- Fingerprint: Redmi/phoenix/phoenix:12/RKQ1.210614.002/V13.0.1.0.SGHCNXM:user/release-keys
- OTA version: 
- Branch: qssi-user-12
12-SKQ1.210908.001-V13.0.1.0.SGHCNXM-release-keys
- Repo: redmi/phoenix
